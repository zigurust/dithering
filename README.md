# README

* Some example program(s) regarding dithering

## References

* [ditherit](https://ditherit.com/)
* [https://ditherit.com/resources/]
* [DHALF.TXT](https://web.archive.org/web/20070927122512/http://www.efg2.com/Lab/Library/ImageProcessing/DHALF.TXT)
* [Quick and dirty image dithering](https://nelari.us/post/quick_and_dirty_dithering/)
* [Floyd Steinberg Dithering](https://en.wikipedia.org/wiki/Floyd%E2%80%93Steinberg_dithering)
