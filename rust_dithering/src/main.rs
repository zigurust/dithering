use image::{GenericImageView, Pixel, Rgb};
use minifb::{Key, KeyRepeat, Window, WindowOptions};
const WIDTH: usize = 625;
const HEIGHT: usize = 625;

const WHITE: u32 = 0x00FFFFFF;

const BLACK: u32 = 0;

const ALPHA_1: u8 = 193;
const ALPHA_2: u8 = 145;

// From http://extremelearning.com.au/unreasonable-effectiveness-of-quasirandom-sequences/
fn plastic_sequence(x: u32, y: u32, luma: u8) -> u32 {
    let threshold = u8::try_from((x * u32::from(ALPHA_1) + y * u32::from(ALPHA_2)) % 256).unwrap();
    let mut color = WHITE;
    if luma < threshold {
        color = BLACK;
    }
    color
}

fn sierra_filter_lite(input_pixel: u8, output_image: &mut Vec<u32>, i: usize) -> u32 {
    let old_pixel = i16::from(input_pixel);
    let new_pixel: i16 = if old_pixel < 127 { 0x00 } else { 0xff };
    let quantization_error = old_pixel - new_pixel;

    let x = i % WIDTH;
    let y = i / WIDTH;

    // first line
    if x != WIDTH - 1 {
        let index = i + 1;
        diffuse_error(index, 2 * quantization_error, output_image);
    }
    // second line
    if y != HEIGHT - 1 {
        if x != 0 {
            let index = i + WIDTH - 1;
            diffuse_error(index, quantization_error, output_image);
        }
        let index = i + WIDTH;
        diffuse_error(index, quantization_error, output_image);
    }

    if new_pixel == 0 {
        BLACK
    } else {
        WHITE
    }
}

fn diffuse_error(index: usize, quantization_error: i16, output_image: &mut Vec<u32>) {
    let pixel = output_image[index];
    #[cfg(test)]
    println!("pixel = {pixel}");
    let rgb = [
        u8::try_from((pixel >> 16) & 0xFF).unwrap(),
        u8::try_from((pixel >> 8) & 0xFF).unwrap(),
        u8::try_from(pixel & 0xFF).unwrap(),
    ];
    #[cfg(test)]
    println!("rgb = {rgb:?}");
    let input_pixel = Rgb::from(rgb).to_luma()[0];
    #[cfg(test)]
    println!("input pixel gray = {input_pixel}");
    #[cfg(test)]
    {
        println!("quantization_error = {quantization_error}");
        let qerr = quantization_error >> 2;
        println!("quantization_error >> 2 = {qerr}");
        let input_pixel_i16 = i16::from(input_pixel);
        println!("input pixel i16 = {input_pixel_i16}");
        let new_pixel_manually = input_pixel_i16 + qerr;
        println!("new_pixel_manually = {new_pixel_manually}");
        let new_pixel = input_pixel_i16 + (quantization_error >> 2);
        println!("new_pixel = {new_pixel}");
    }
    let new_neighbour_pixel = i16::from(input_pixel) + (quantization_error >> 2);
    #[cfg(test)]
    println!("new_neighbour_pixel = {new_neighbour_pixel}");
    let new_neighbour_pixel = std::cmp::max(0, std::cmp::min(0xff, new_neighbour_pixel));
    #[cfg(test)]
    println!("clamped(new_neighbour_pixel) = {new_neighbour_pixel}");

    let new_neighbour_pixel_grayscale = u8::try_from(new_neighbour_pixel).expect(
        format!("Checked value {new_neighbour_pixel} against 0 and 255")
            .to_string()
            .as_str(),
    );

    #[cfg(test)]
    println!(
        "new neighbour pixel={new_neighbour_pixel}. grayscale={new_neighbour_pixel_grayscale}"
    );

    output_image[index] = u32::from(new_neighbour_pixel_grayscale) << 16
        | u32::from(new_neighbour_pixel_grayscale) << 8
        | u32::from(new_neighbour_pixel_grayscale);
}

#[test]
fn test_diffuse_error() {
    let mut output_image: Vec<u32> = vec![0; 1];
    let quantization_error = -127;
    diffuse_error(0, quantization_error, &mut output_image);
    assert_eq!(-32, quantization_error >> 2);
    assert_eq!(output_image[0], 0);
    output_image[0] = 27;
    let quantization_error = 8;
    diffuse_error(0, quantization_error, &mut output_image);
    assert_eq!(3, output_image[0] & 0xFF);
}

#[derive(PartialEq)]
enum ImageType {
    Original,
    Grayscale,
    PlasticSequenceDither,
    SerriaFilterLite,
}

fn main() {
    let img = image::open("test_image_cropped.png").unwrap();
    let grayscale: Vec<u8> = img.to_luma8().into_raw();
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

    let mut window = Window::new(
        "Test - ESC to exit",
        WIDTH,
        HEIGHT,
        WindowOptions::default(),
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    // Limit to max ~60 fps update rate
    window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));

    let mut image_type = ImageType::Original;

    while window.is_open() && !window.is_key_down(Key::Escape) {
        if window.is_key_pressed(Key::P, KeyRepeat::No) {
            image_type = ImageType::PlasticSequenceDither;
        } else if window.is_key_pressed(Key::G, KeyRepeat::No) {
            image_type = ImageType::Grayscale;
        } else if window.is_key_pressed(Key::O, KeyRepeat::No) {
            image_type = ImageType::Original;
        } else if window.is_key_pressed(Key::S, KeyRepeat::No) {
            image_type = ImageType::SerriaFilterLite;
        }

        for (i, pixel) in img.pixels().enumerate() {
            let rgb_pixel: Rgb<u8> = pixel.2.to_rgb();

            let value;
            match image_type {
                ImageType::PlasticSequenceDither => {
                    let luma = rgb_pixel.to_luma();
                    let x = u32::try_from(i % WIDTH).unwrap();
                    let y = u32::try_from(i / WIDTH).unwrap();
                    value = plastic_sequence(x, y, luma.channels()[0]);
                }
                ImageType::Grayscale => {
                    let luma = rgb_pixel.to_luma();
                    let luma_value = luma.channels()[0];
                    value = u32::from(luma_value) << 16
                        | u32::from(luma_value) << 8
                        | u32::from(luma_value);
                }
                ImageType::Original => {
                    let channels = rgb_pixel.channels();
                    value = u32::from(channels[0]) << 16
                        | u32::from(channels[1]) << 8
                        | u32::from(channels[2]);
                }
                ImageType::SerriaFilterLite => {
                    if i == 0 {
                        let luma = rgb_pixel.to_luma()[0];
                        buffer[i] = u32::from(luma) >> 16 | u32::from(luma) >> 8 | u32::from(luma);
                    }
                    let pixel = buffer[i];
                    let rgb = [
                        u8::try_from(pixel >> 16 & 0xFF).unwrap(),
                        u8::try_from(pixel >> 8 & 0xFF).unwrap(),
                        u8::try_from(pixel & 0xFF).unwrap(),
                    ];
                    let current_grayscale_pixel = Rgb::from(rgb).to_luma()[0];
                    value = sierra_filter_lite(current_grayscale_pixel, &mut buffer, i);
                }
            };
            buffer[i] = value;
        }

        // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
        window.update_with_buffer(&buffer, WIDTH, HEIGHT).unwrap();
    }
}
